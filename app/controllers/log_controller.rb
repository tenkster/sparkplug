class LogController < ApplicationController
  before_filter :authenticate_user!
  helper_method :find_events
  
  def find_events(block)
    c = @timeblocks.find {|t| t.start_time == block }
    unless c.nil?
      c.events_from(current_user)
    else
      nil
    end
  end

  def index
    current_user.reload
    @timeblocks = current_user.time_blocks.all
    #binding.pry
  end

end
