class UsersController < ApplicationController
  before_filter :authenticate_admin!, :except => [:show]
  def index
  end

  def show
    @user = User.find_by_email(params[:id])
  end
end
