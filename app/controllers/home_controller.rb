class HomeController < ApplicationController
  def index
    if user_signed_in?
      redirect_to controller: 'log', action: 'index'
    end
  end
end
