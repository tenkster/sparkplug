class CreateTimeBlocks < ActiveRecord::Migration
  def change
    create_table :time_blocks do |t|
      t.belongs_to :user, index: true
      t.timestamp :start_time
      t.timestamp :end_time
      t.integer :score
      t.timestamps
    end
  end
end
