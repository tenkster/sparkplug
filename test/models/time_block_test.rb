require 'test_helper'

class TimeBlockTest < ActiveSupport::TestCase

  test "Time Block saves to the database" do
    t = TimeBlock.new(start_time: Time.new(2014, 8, 29, 0, 0, 0), score: 0)
    t.save
    assert_equal Time.new(2014, 8, 29, 0, 0, 0), TimeBlock.last.start_time
    assert_equal Time.new(2014, 8, 29, 0, 0, 0) + 30.minutes, TimeBlock.last.end_time
  end

  test "Save event tags to database" do
    t = TimeBlock.new(start_time: Time.new(2014, 8, 29, 0, 0, 0), score: 0)
    t.event_list.add("Swimming", "Jogging")
    t.save
    t.reload
    assert_equal TimeBlock.last.event_list, ["Jogging", "Swimming"]
  end
end
