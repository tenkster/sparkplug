	/*! bigSlide - v0.5.0 - 2014-09-12
* http://ascott1.github.io/bigSlide.js/
* Copyright (c) 2014 Adam D. Scott; Licensed MIT */
/*! bigSlide - v0.4.3 - 2014-01-25
* http://ascott1.github.io/bigSlide.js/
* Copyright (c) 2014 Adam D. Scott; Licensed MIT */

(function($) {
  'use strict';

  $.fn.bigSlideR = function(options) {

    var settings = $.extend({
      'menu2': ('#menu2'),
      'push': ('.push'),
      'side': 'right',
      'menu2Width': '15.625em',
      'speed': '300',
      'activeBtn':'menu2-open'
    }, options);

    var menu2Link = this,
        menu2 = $(settings.menu2),
        push = $(settings.push),
        width = settings.menu2Width;

    var positionOffScreen = {
      'position': 'fixed',
      'top': '0',
      'bottom': '0',
      'settings.side': '-' + settings.menu2Width,
      'width': settings.menu2Width,
      'height': '100%'
    };

    var animateSlide = {
      '-webkit-transition': settings.side + ' ' + settings.speed + 'ms ease',
      '-moz-transition': settings.side + ' ' + settings.speed + 'ms ease',
      '-ms-transition': settings.side + ' ' + settings.speed + 'ms ease',
      '-o-transition': settings.side + ' ' + settings.speed + 'ms ease',
      'transition': settings.side + ' ' + settings.speed + 'ms ease'
    };

    menu2.css(positionOffScreen);
    push.css(settings.side, '0');
    menu2.css(animateSlide);
    push.css(animateSlide);

    menu2._state = 'closed';

    menu2.open = function() {
      menu2._state = 'open';
      menu2.css(settings.side, '0');
      push.css(settings.side, width);
      menu2Link.addClass(settings.activeBtn);
    };

    menu2.close = function() {
      menu2._state = 'closed';
      menu2.css(settings.side, '-' + width);
      push.css(settings.side, '0');
      menu2Link.removeClass(settings.activeBtn);
    };

   $(document).on('click.bigSlideR', function(e) {
     if (!$(e.target).parents().andSelf().is(menu2Link) && menu2._state === 'open')  {
       menu2.close();
       menu2Link.removeClass(settings.activeBtn);
     }
    });

    menu2Link.on('click.bigSlideR', function(e) {
      e.preventDefault();
      if (menu2._state === 'closed') {
        menu2.open();
      } else {
        menu2.close();
      }
    });

    menu2Link.on('touchend', function(e){
      menu2Link.trigger('click.bigSlideR');
      e.preventDefault();
    });

    return menu2;

  };

}(jQuery));