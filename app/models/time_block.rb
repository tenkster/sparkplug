class TimeBlock < ActiveRecord::Base
  belongs_to :user
  acts_as_taggable_on :events
  validates :start_time, uniqueness: {scope: :user_id}
  
  #validates :score, numericality: { less_than_or_equal_to: 1, greater_than_or_equal_to: -1}
  #validates :start_time, presence: true
  before_save :set_end_time

  #TODO: validate intervals so that you can only create 30 minute intervals for each time-block
  
  #TODO: validate score so they can only be edited after the timeblock has already occured.
  def set_end_time
    self.end_time = self.start_time + 30.minutes
  end
end
