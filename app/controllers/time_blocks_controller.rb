class TimeBlocksController < ApplicationController
  
  respond_to :js ,:json


  ## This function is called whenever the field gets updated on the front end ####
  def update_timeblock
    timeblock = current_user.time_blocks.find_by(start_time: time_block_params[:start_time])
    #binding.pry
    if (time_block_params[:add_timeblock] == "true") # things we do if were adding an event to timeblock
      if (timeblock.nil?) 
        timeblock = create_timeblock
        flash[:notice] = "#{current_user.username} successfully created a new timeblock."
      end
      add_event_to(timeblock)
    else ### things we do if were taking an event away from timeblock                   
      remove_event_from(timeblock) 
      timeblock.save 
      timeblock.reload 
      if (timeblock.events_from(current_user).empty?)
        timeblock.delete
      end
    end
  end 

  def add_event_to(block)
    block.events_from(current_user).add(time_block_params[:events])
    block.save
    flash[:notice] = "#{current_user.username} successfully saved #{time_block_params[:events]} 
    to #{time_block_params[:start_time]}"
    respond_to do |format|
      format.json {render json: "okay", status: :ok}
    end
  end

  def remove_event_from(block)
    block.events_from(current_user).remove(time_block_params[:events].strip)
    respond_to do |format|
      format.json {render json: "okay", status: :ok}
    end
  end

  def create_timeblock
    TimeBlock.create(user: current_user, start_time: time_block_params[:start_time], score: time_block_params[:score])
  end


  private
  def time_block_params
    params.require(:time_block).permit!
  end
end