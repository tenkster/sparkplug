require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "recieve user tags objects when we use the function owned_tags" do
    @user = User.new(email: "email-guy@anemail.com", password: "password1111", username: "tester")
    @user.save
    @timeblock = TimeBlock.new(user: @user, start_time: DateTime.now, score: 0)
    @timeblock.save
    @user.tag(@timeblock, with: "Swimming", on: :events)
    @user.save
    @user.reload
    #binding.pry
    assert_equal @user.owned_tags, [ActsAsTaggableOn::Tag.new(id: 2, name: "Swimming", taggings_count: 1)]
  end

  test "Recieve the name of every tag user has ever used using our own function" do
    @user = User.new(email: "email-guy@anemail.com", password: "password1111", username: "tester")
    @user.save
    @timeblock = TimeBlock.new(user: @user, start_time: DateTime.now, score: 0)
    @timeblock.save
    @user.tag(@timeblock, with: "Swimming, Jogging", on: :events)
    @user.save
    @user.reload
    binding.pry
    assert_equal @user.get_all_events, ["Swimming", "Jogging"]
  end



  test "Take all the user tags and store them in a hash, where the key is time and the value is an array of tags" do
    # basically if we hash out these values in this fashion, we can access them
    # from the front-end incredibly efficiently and 
    # have a simpler means of calling them
    # in each selectize field on page load.
    # used my birthday and the time 730
    assert @user.hash_all_events_by_time, { 092319900730: ["being born"], 092319911200: ["first words"]}
  end
end
